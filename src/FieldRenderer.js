import React from "react"
import { propOr, map, pipe, merge } from "f-utility"
import StyledFieldWithError from "./StyledFieldWithError"

const FieldRenderer = ({
  inputs,
  placeholder,
  bem,
  type,
  name,
  question,
  options,
  errors
}) => {
  const state = propOr(false, name, errors) ? [type, `error`] : type

  return (
    <div
      key={name}
      className={bem(
        "form-field",
        state
        // [type].concat(propOr("", name, errors) ? `error` : ``)
      )}
    >
      {inputs && (
        <label className={bem("field-label", "nested")}>{question}</label>
      )}
      <div className={bem("form-field-content", inputs ? `nested` : null)}>
        {inputs ? (
          map(
            pipe(
              merge({ placeholder, bem, nested: name, errors }),
              StyledFieldWithError
            ),
            inputs
          )
        ) : (
          <StyledFieldWithError
            {...{ placeholder, errors, bem, type, name, options, question }}
          />
        )}
      </div>
    </div>
  )
}

export default FieldRenderer
