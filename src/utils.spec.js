import { random } from "f-utility"
import { wrapWithKey } from "./utils"

test("wrapWithKey", () => {
  const word = random.word(10)
  expect(wrapWithKey(`key`, word.split(``))).toEqual({
    key: word.split(``)
  })
})
