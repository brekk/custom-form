import { trace } from "xtrace"

import {
  isObject,
  keys,
  K,
  prop,
  fromPairs,
  entries,
  reject,
  pipe,
  map,
  reduce,
  merge,
  curry
} from "f-utility"
import { wrapWithKey } from "./utils"

export const convertOptionsToInitialValues = curry((name, options) =>
  pipe(
    map(K("")),
    wrapWithKey(name)
  )(options)
)

export const convertInputsToInitialValues = curry((name, inputs) =>
  pipe(
    map(x => [x.name, x.value]),
    reduce((y, [name, value]) => merge(y, { [name]: value || "" }), {}),
    wrapWithKey(name)
  )(inputs)
)

export const buildInitialValuesFromFields = pipe(
  map(x => {
    if (x.options && (x.type === "radio" || x.type === "checkbox")) {
      return convertOptionsToInitialValues(x.name, x.options)
    }
    if (x.inputs) {
      return convertInputsToInitialValues(x.name, x.inputs)
    }
    return wrapWithKey(x.name, "")
  }),
  reduce(merge, {})
)
export const validator = curry((ref, [key, value]) => {
  const base = ref.form[key]
  return [key, base && base.validate ? base.validate(value) : null]
})

export const validate = curry((data, values) =>
  pipe(
    entries,
    map(validator(data)),
    reject(([x, y]) => y === false || (isObject(y) && keys(y).length === 0)),
    fromPairs
  )(values)
)
