import {
  convertOptionsToInitialValues,
  convertInputsToInitialValues,
  buildInitialValuesFromFields,
  validator,
  validate
} from "./logic"
import data from "./data.fixture"

test("validate", () => {
  const data = { form: { a: { validate: () => `bad` } } }
  const values = { a: 100 }
  expect(validate(data, values)).toEqual({ a: `bad` })
})

test(`validator`, () => {
  const ref = { form: { a: { validate: () => `bad` } } }
  const input = [`a`, `y`]
  expect(validator(ref, input)).toEqual([`a`, `bad`])
  expect(validator(ref, [`b`, `b`])).toEqual([`b`, null])
})
const options = { a: 1, b: 2, c: 3 }
const inputs = [{ name: `a` }, { name: `b` }, { name: `c` }]
test(`convertOptionsToInitialValues`, () => {
  expect(convertOptionsToInitialValues(`test`, options)).toEqual({
    test: { a: "", b: "", c: "" }
  })
})

test(`convertInputsToInitialValues`, () => {
  expect(convertInputsToInitialValues(`test`, inputs)).toEqual({
    test: { a: "", b: "", c: "" }
  })
})
test(`buildInitialValuesFromFields`, () => {
  const form = {
    x: { name: "x", inputs },
    y: {
      name: "y",
      type: "checkbox",
      options
      // options: { a: { name: "a" }, b: { name: "b" }, c: { name: "c" } }
    },
    z: { name: "z" }
  }
  expect(buildInitialValuesFromFields(form)).toEqual({
    x: { a: "", b: "", c: "" },
    y: { a: "", b: "", c: "" },
    z: ""
  })
})
