import { curry, pipe, reduce, isFunction } from "f-utility"

export const wrapWithKey = curry((key, x) => ({ [key]: x }))
