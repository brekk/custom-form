import React, { Fragment } from "react"
import { prop, isPOJO, keys, pipe, merge } from "f-utility"
import { Field, ErrorMessage } from "formik"
import { ReactComponent as ErrorIcon } from "./icon-warning.svg"
import Options from "./Options"

const StyledFieldWithError = ({
  placeholder,
  type,
  name,
  bem,
  question,
  options,
  nested = ``
}) => {
  const optionLength = (options && isPOJO(options) && keys(options).length) || 0
  const willRenderOptions = optionLength > 0
  const isNested = nested.length > 0
  const nestedRef = `${isNested ? nested + `.` : ``}${name}`
  const body = willRenderOptions ? (
    pipe(
      merge({ bem, type, name, options }),
      Options
    )({})
  ) : (
    <>
      <Field
        placeholder={placeholder}
        type={type}
        name={nestedRef}
        className={bem("field", type)}
      />
    </>
  )

  return (
    <Fragment key={name}>
      <label className={bem("field-label", type)}>{question}</label>
      {body}
      <ErrorMessage name={nestedRef} component="div">
        {error => (
          <div className={bem("field-error", type)}>
            <ErrorIcon className={bem("field-error-icon")} />
            {error}
          </div>
        )}
      </ErrorMessage>
    </Fragment>
  )
}

export default StyledFieldWithError
