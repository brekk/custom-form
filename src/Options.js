import React from "react"
import { pipe, entries, map } from "f-utility"
import { Field } from "formik"

const Options = ({ bem, name, type, options }) => (
  <ul className={bem("field-options")}>
    {options &&
      pipe(
        entries,
        map(([k, v]) => (
          <li className={bem("field-option-wrapper")} key={k}>
            <Field
              type={type}
              name={`${name}.${k}`}
              className={bem(`field-option`, type)}
            />
            <label className={bem("field-option-label")}>{v}</label>
          </li>
        ))
      )(options)}
  </ul>
)
export default Options
