import { pipe, entries, filter } from "f-utility"

const data = {
  title: `Job Criteria`,
  explanation: `We will use the data we collect here to better target your desired audience`,
  form: {
    experience: {
      question: `A minimum No. years of experience`,
      type: `text`,
      placeholder: `e.g. 5+`,
      validate: v => (v.length > 0 ? false : `The field is required`)
    },
    education: {
      question: `Level of education`,
      type: `checkbox`,
      validate: pipe(
        entries,
        filter(([k, y]) => y !== ""),
        y => y.length === 0 && `Please select at least one education level`
      ),
      options: {
        highschool: `GCSE / A-Level / High-school / GED`,
        diploma: `Vocational / Diploma / Associates degree`,
        graduate: `Bachelor / Graduate`,
        postgrad: `Master / Post-Graduate / PhD`
      }
    },
    hours: {
      question: `No. of working hours (per week)`,
      type: "number",
      validate: ({ min, max }) => {
        const errors = {}
        if (min < 1) {
          errors.min = `Expected a minimum value above 0`
        }
        if (min > 168) {
          errors.min = `Minimum is larger than the number of hours in a week`
        }
        if (max) {
          if (max < min) {
            errors.max = `Maximum must be larger than or equal to minimum`
          }
          if (max > 168) {
            errors.max = `Maximum is larger than the number of hours in a week`
          }
        } else {
          errors.max = `Expected a maximum value above 0`
        }
        return errors
      },
      inputs: [
        {
          name: `min`,
          question: `Min.`,
          type: `number`,
          value: 32
        },
        {
          name: `max`,
          question: `Max.`,
          type: `number`,
          value: 40
        }
      ]
    }
  }
}
export default data
