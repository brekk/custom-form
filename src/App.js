import React from "react"
import blem from "blem"
import { withState } from "recompose"
import {
  keys,
  filter,
  curry,
  map,
  reject,
  pipe,
  entries,
  merge,
  prop
} from "f-utility"
import { trace } from "xtrace"
import { Formik, Form } from "formik"
import { buildInitialValuesFromFields, validate } from "./logic"
import { ReactComponent as SuccessIcon } from "./icon-done.svg"
import FieldRenderer from "./FieldRenderer"
import data from "./data.fixture"
import "./App.scss"

const createFormData = pipe(
  prop("form"),
  entries,
  map(([name, x]) => merge(x, { name }))
)

const formData = createFormData(data)

const onSubmitDefault = curry((props, values) => {
  const { setEditing, setRawForm, setForm } = props
  const cloned = merge({}, values)
  console.log(values, `values!`)
  cloned.education = reject(x => x === "", cloned.education)
  setEditing(false)
  setRawForm(values)
  setForm(cloned)
})
console.log(`formData`, formData)

const Saved = ({ bem, values, setEditing }) => {
  return (
    <div className={bem("saved-content")}>
      <button
        className={bem("button", "edit")}
        onClick={() => setEditing(true)}
      >
        Edit
      </button>
      {pipe(
        entries,
        // trace(`>>>>`),
        map(([k, v]) => [k, formData.find(y => y.name === k), v]),
        map(([k, l, v]) => {
          // this is not as clean as I would like
          if (k === "hours") {
            return (
              <div className={bem("question", l.type)}>
                <strong>{l.question}</strong>
                <span>{`${v.min} – ${v.max} hours`}</span>
              </div>
            )
          }
          if (l.type === "checkbox") {
            return (
              <div className={bem("question", l.type)}>
                <strong>{l.question}</strong>
                <span>
                  {pipe(
                    entries,
                    trace(`!!! ${k}`),
                    filter(([kk]) => keys(v).includes(kk)),
                    trace(`:::`),
                    map(([p, q]) => <div key={p}>{q}</div>)
                  )(l.options)}
                </span>
              </div>
            )
          }
          return (
            <div className={bem("question", l.type)}>
              <strong>{l.question}</strong>
              <span>{v}</span>
            </div>
          )
        })
      )(values)}
    </div>
  )
}
const App = props => {
  console.log(props)
  const {
    fields = formData,
    bem = blem("App"),
    onSubmit = onSubmitDefault,
    setEditing,
    form,
    rawForm,
    editing
  } = props
  return (
    <section className={bem()}>
      <header className={bem("header")}>
        {editing ? (
          <div className={bem("index", data.index || 1)}>{data.index || 1}</div>
        ) : (
          <SuccessIcon />
        )}
        <h1 className={bem("title")}>{data.title}</h1>
        {editing && <em className={bem("explanation")}>{data.explanation}</em>}
      </header>
      {editing ? (
        <Formik
          validate={validate(data)}
          initialValues={
            keys(rawForm).length > 0
              ? rawForm
              : buildInitialValuesFromFields(fields)
          }
          onSubmit={onSubmit(props)}
        >
          {pipe(({ errors, isSubmitting }) => (
            <Form>
              {map(
                pipe(
                  merge({ bem, errors }),
                  FieldRenderer
                )
              )(fields)}
              <div className={bem("control-panel")}>
                <button className={bem("button", "submit")} type="submit">
                  Save
                </button>
              </div>
            </Form>
          ))}
        </Formik>
      ) : (
        <>
          <Saved bem={bem} values={form} setEditing={setEditing} />
        </>
      )}
    </section>
  )
}
export const decorators = pipe(
  withState("form", "setForm", {}),
  withState("rawForm", "setRawForm", {}),
  withState("editing", "setEditing", true)
)
export default decorators(App)
