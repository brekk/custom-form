# vonq test

`yarn start` will run this application locally.

Choices:

* For simplicity of config, [create-react-app](//npmjs.org/package/create-react-app)
* For the forms themselves, [formik](//npmjs.org/package/formik)
* To utilize the BEM notation pattern: [blem](//npmjs.org/package/blem)
  - this is a module I have published and maintain.
* For separation of logic from view concerns: [recompose](//npmjs.org/package/recompose)
* For straightforward logging as a side-effect: [xtrace](//npmjs.org/package/xtrace)
  - this is a module I have published and maintain.
* For composed functions and general toolbelt utilities: [f-utility](//npmjs.org/package/f-utility)
  - this is a module I have published and maintain.

Issues:

I experienced a few issues around deploying. I'm using zeit's [now](https://github.com/zeit/now-examples/tree/master/create-react-app) tool in conjunction with create-react-app, and while I've experienced completely painless deploys before, this time I saw first a problem in file sizes over limits, then issues around what appear to be routing. Edit: I was able to resolve the problem by changing the `now.json` file, but the existing [documented solution](https://github.com/zeit/now-examples/blob/master/create-react-app/now.json) doesn't work.

If I had more time I would have pulled the sub components out of App.js and cleaned out some of the places where I'm relying on a global (`data`, exported from './src/constants.js`). Additionally, I wasn't able to style the edit screen as well as I would have liked.
